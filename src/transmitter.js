import { EventEmitter } from 'events';
var Transmitter = Object.assign({}, EventEmitter.prototype, { /* ... */ });
export default Transmitter;