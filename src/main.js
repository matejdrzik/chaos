import Vue from 'vue'
import VueRouter from 'vue-router';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-default/index.css';
import App from './App.vue';
import Welcome from './Welcome.vue';
import Beacons from './Beacons.vue';
import Beacon from './Beacon.vue';
import Users from './Users.vue';
import User from './User.vue';

Vue.use(ElementUI);
Vue.use(VueRouter);

const routes = [
  { path: '/', component: Welcome },
  { path: '/beacons', component: Beacons },
  { path: '/beacons/:id', component: Beacon, name: 'Beacon' },
  { path: '/users', component: Users, name },
  { path: '/users/:id', component: User, name }
];

const router = new VueRouter({ routes });

new Vue({
  router,
  el: '#app',
  render: h => h(App)
});


